import expect from 'expect';
import reducer from '../../app/src/reducers/_inc/tasks';
import { FETCH_TASKS, ADD_TASK, UPDATE_TASK, DELETE_TASK } from '../../app/src/actions/tasks';

describe('tasks reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual([])
  });

  it('FETCH_TASKS should return an updated state based on passed payload', () => {
    expect(reducer({}, {
      type: FETCH_TASKS,
      payload: [
        {
          'priority': 1,
          'description': 'create JSON fixtures for stories',
          'status': 'to do'
        }
      ]
    })).toEqual(
      [
        {
          'priority': 1,
          'description': 'create JSON fixtures for stories',
          'status': 'to do'
        }
      ]
      )
  });

  it('ADD_STORY should return an updated state based on the passed payload', () => {
    expect(reducer([
      {
        'priority': 1,
        'description': 'create JSON fixtures for stories',
        'status': 'to do'
      }
    ], {
      type: ADD_TASK,
      payload:
        {
          'priority': 2,
          'description': 'create JSON fixtures for stories',
          'status': 'to do'
        }

    })).toEqual(
      [
        {
          'priority': 1,
          'description': 'create JSON fixtures for stories',
          'status': 'to do'
        },
        {
          'priority': 2,
          'description': 'create JSON fixtures for stories',
          'status': 'to do'
        }
      ]
    )
  });

  it('UPDATE_STORY should return an updated state based on passed payload', () => {
    expect(reducer([
      {
        "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
        'priority': 1,
        'description': 'create JSON fixtures for stories',
        'status': 'to do'
      }
    ], {
      type: UPDATE_TASK,
      payload:
        {
          "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
          "priority": 2,
          "description": "create JSON fixtures for stories",
          "status": "to do"
        }
    })).toEqual(
      [
        {
          "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
          "priority": 2,
          "description": "create JSON fixtures for stories",
          "status": "to do"
        }
      ]
    )
  });

  it('DELETE_STORY should remove story from state based passed payload id', () => {
    expect(reducer([
      {
        "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
        "priority": 2,
        "description": "create JSON fixtures for stories",
        "status": "to do"
      }
    ], {
      type: DELETE_TASK,
      payload:
      {
        "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c'
      }
    })).toEqual([])
  });
});
