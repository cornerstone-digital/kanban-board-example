import expect from 'expect';
import reducer from '../../app/src/reducers/_inc/stories';
import { FETCH_STORIES, ADD_STORY, UPDATE_STORY, DELETE_STORY } from '../../app/src/actions/stories';

describe('stories reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual([])
  });

  it('FETCH_STORIES should return an updated state based on passed payload', () => {
    expect(reducer({}, {
      type: FETCH_STORIES,
      payload: [
        {
          "priority": 1,
          "persona": "product owner",
          "feature": "create new stories in the backlog",
          "justification": "request features"
        }
      ]
    })).toEqual(
      [
        {
          "priority": 1,
          "persona": "product owner",
          "feature": "create new stories in the backlog",
          "justification": "request features"
        }
      ]
      )
  });

  it('ADD_STORY should return an updated state based on the passed payload', () => {
    expect(reducer([
      {
        "priority": 1,
        "persona": "product owner",
        "feature": "create new stories in the backlog",
        "justification": "request features"
      }
    ], {
      type: ADD_STORY,
      payload:
        {
          "priority": 2,
          "persona": "product owner",
          "feature": "Some new feature",
          "justification": "request features"
        }

    })).toEqual(
      [
        {
          "priority": 1,
          "persona": "product owner",
          "feature": "create new stories in the backlog",
          "justification": "request features"
        },
        {
          "priority": 2,
          "persona": "product owner",
          "feature": "Some new feature",
          "justification": "request features"
        }
      ]
    )
  });

  it('UPDATE_STORY should return an updated state based on passed payload', () => {
    expect(reducer([
      {
        "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
        "priority": 1,
        "persona": "product owner",
        "feature": "create new stories in the backlog",
        "justification": "request features"
      }
    ], {
      type: UPDATE_STORY,
      payload:
        {
          "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
          "priority": 2,
          "persona": "product owner",
          "feature": "create new stories in the backlog",
          "justification": "request features"
        }
    })).toEqual(
      [
        {
          "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
          "priority": 2,
          "persona": "product owner",
          "feature": "create new stories in the backlog",
          "justification": "request features"
        }
      ]
    )
  });

  it('DELETE_STORY should remove story from state based passed payload id', () => {
    expect(reducer([
      {
        "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c',
        "priority": 1,
        "persona": "product owner",
        "feature": "create new stories in the backlog",
        "justification": "request features"
      }
    ], {
      type: DELETE_STORY,
      payload:
      {
        "id": '52ef15d0-a40a-440c-b71b-b3b41fb13a3c'
      }
    })).toEqual([])
  });
});
