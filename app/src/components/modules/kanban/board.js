import React, { Component } from 'react';
import { connect } from 'react-redux';

import StoryRow from './_partials/StoryRow';

class Board extends Component {
  filterTasksByStoryId(storyId) {
    return this.props.tasks.filter(task => task.story_id === storyId);
  }

  render() {

    return (
      <div className="container">
        <div className="col-md-12">
          <div className="row">
            <table width="100%">
              <tbody>
                <tr>
                  <th>Story</th>
                  <th>To Do</th>
                  <th>In Progress</th>
                  <th>Done</th>
                </tr>
                  {this.props.stories.map((story) => (
                    <StoryRow key={story.id} story={story} tasks={this.filterTasksByStoryId(story.id)} />
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { stories: state.stories, tasks: state.tasks };
}

export default connect(mapStateToProps)(Board);
