import React, { Component } from 'react';
import { Link } from 'react-router';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { deleteTask } from '../../../../actions/tasks';

class TaskCard extends Component {
  static contextTypes = {
    store: React.PropTypes.object
  };

  constructor(props) {
    super(props);
  }

  deleteTask() {
    this.props.deleteTask(this.props.task.id);
  }

  render() {
    return (
      <div className="panel panel-default">
        <div className="panel-heading"></div>
        <div className="panel-body">
          <p>{this.props.task.description}</p>
          <p>Priority: {this.props.task.priority}</p>
          <Link to={'/tasks/' + this.props.task.id} className="btn btn-primary">Edit</Link>
          <Button className="btn btn-danger" onClick={this.deleteTask.bind(this)}>Delete</Button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {

  };
}

export default connect(mapStateToProps, { deleteTask })(TaskCard);
