import React, { Component } from 'react';
import { Input, ButtonInput } from 'react-bootstrap';
import { connect } from 'react-redux';
import { fetchTasks, addTask, updateTask } from '../../../actions/tasks';
import KanbanService from '../../../services/KanbanService';

class Task extends Component {
  static contextTypes = {
    router: React.PropTypes.object,
    store: React.PropTypes.object
  };

  state = {
    canSubmit: false,
    buttonValue: 'Add Task',
    task: {
      story_id: this.props.params.storyId,
      priority: '1',
      description: '',
      status: 'to do'
    }
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchTasks();

    if(this.props.params.taskId) {
      this.setState({
        buttonValue: 'Save Task',
        task: this.context.store.getState().tasks.filter(task => task.id === this.props.params.taskId)[0]
      }, () => {
        this.validateForm();
      });
    }

    this.validateForm();
  }

  onPriorityChange(e) {
    this.setState({
      task: {
        ...this.state.task,
        priority: e.target.value
      }
    }, () => {
      this.validateForm();
    });
  }

  onDescriptionChange(e) {
    this.setState({
      task: {
        ...this.state.task,
        description: e.target.value
      }
    }, () => {
      this.validateForm();
    });
  }

  onStatusChange(e) {
    this.setState({
      task: {
        ...this.state.task,
        status: e.target.value
      }
    }, () => {
      this.validateForm();
    });
  }

  validateForm() {
    let valid = true;

    if(this.state.task.priority.length === 0) {
      valid = false;
    }

    if(this.state.task.description.length === 0) {
      valid = false;
    }

    if(this.state.task.status.length === 0) {
      valid = false;
    }

    if(valid) {
      this.enableButton();
    }
    else {
      this.disableButton();
    }
  }

  disableButton() {
    this.setState({
      canSubmit: false
    });
  }

  enableButton() {
    this.setState({
      canSubmit: true
    });
  }

  submit(e) {
    e.preventDefault();

    if(this.props.params.taskId) {
      this.props.updateTask(this.state.task);
    } else {
      KanbanService.addTask(this.state.task)
        .then(taskId => {
          return KanbanService.getTaskById(taskId);
        })
        .then(task => {
          this.props.addTask(task);

          this.context.router.push('/');
        });
    }
  }

  render() {
    var priorities = [
      {value: '1', label: 'Must Have'},
      {value: '2', label: 'Nice To Have'},
      {value: '3', label: 'Low Priority'}
    ];

    var statuses = [
      {value: 'to do', label: 'To Do'},
      {value: 'in progress', label: 'In Progress'},
      {value: 'done', label: 'Done'}
    ];

    return (
      <section className="story">
        <h1>Task</h1>
        <form onSubmit={this.submit.bind(this)} onChange={this.validateForm.bind(this)}>
          <Input name="priority" ref="priority" type="select" label="Priority" value={this.state.task.priority} onChange={this.onPriorityChange.bind(this)} required>
            {priorities.map((priority, index) => (
              <option key={index} value={priority.value}>{priority.value}: {priority.label}</option>
            ))}
          </Input>
          <Input name="description" ref="description" type="textarea" label="Description" value={this.state.task.description} onChange={this.onDescriptionChange.bind(this)} required/>
          <Input name="status" ref="status" type="select" label="Status" value={this.state.task.status} onChange={this.onStatusChange.bind(this)} required>
            {statuses.map((status, index) => (
              <option key={index} value={status.value}>{status.label}</option>
            ))}
          </Input>
          <ButtonInput type="submit" value={this.state.buttonValue} disabled={!this.state.canSubmit} />
        </form>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    tasks: state.tasks
  };
}

export default connect(mapStateToProps, { fetchTasks, addTask, updateTask })(Task);
