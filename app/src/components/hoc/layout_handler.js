import React, { Component } from 'react';

export default function (ComposedComponent, Layout) {
  class LayoutHandler extends Component {
    static contextTypes = {
      router: React.PropTypes.object,
    };

    render() {
      return (
        <Layout>
          <ComposedComponent {...this.props} />
        </Layout>
      );
    }
  }

  return LayoutHandler;
}
