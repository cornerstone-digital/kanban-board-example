import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class Header extends Component {
  render() {
    return (
      <header className="navbar navbar-default navbar-fixed-top">
        <div className="navbar-header">
          <button className="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".navbar-collapse">
            <i className="icon-reorder"></i>
          </button>
          <a className="navbar-brand"  href="/">Kanban Board</a>
        </div>
        <nav className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li>
              <Link to="/">View Board</Link>
            </li>
            <li>
              <Link to="/story/add">Add Story</Link>
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header;
