import React, { Component, PropTypes } from 'react';
import Header from './partials/header';
import 'styles/frontend/style.scss';
import $ from 'jquery';

export default class MainLayout extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        {this.props.children}
      </div>
    );
  }
}
