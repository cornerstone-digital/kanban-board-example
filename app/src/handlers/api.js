import axios from 'axios';
import config from '../config';

class ApiHandler {
  constructor() {
    this.client = axios.create({
      baseURL: config.api.baseURL + config.api.apiVersion
    });
  }

  callApi(url, method, requestContent) {
    return this.client[method](url, requestContent)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        const errorMessage = {
          title: 'API Error',
          message: error.status + ': ' + error.data.error || error.statusText,
          level: 'error'
        };

        return errorMessage;
      });
  }

  get(url, requestContent) {
    return this.callApi(url, 'get', requestContent);
  }

  post(url, requestContent) {
    return this.callApi(url, 'post', requestContent);
  }

  delete(url, requestContent) {
    return this.callApi(url, 'delete', requestContent);
  }

  patch(url, requestContent) {
    return this.callApi(url, 'patch', requestContent);
  }
}

export default new ApiHandler();
