export const FETCH_TASKS = 'tasks/fetch_tasks';
export const ADD_TASK = 'tasks/add';
export const UPDATE_TASK = 'tasks/update';
export const DELETE_TASK = 'tasks/delete';
export const TASK_ADDED = 'tasks/added';

import KanbanService from '../services/KanbanService';

export function fetchTasks(options) {
  return {
    type: FETCH_TASKS,
    payload: KanbanService.getTasks(options)
  };
}

export function addTask(task) {
  return {
    type: ADD_TASK,
    payload: task
  };
}

export function updateTask(task) {
  return {
    type: UPDATE_TASK,
    payload: KanbanService.updateTask(task.id, task)
  };
}

export function deleteTask(task_id) {
  return {
    type: DELETE_TASK,
    payload: KanbanService.deleteTask(task_id),
    successAction: {
      type: DELETE_TASK,
      payload: {
        id: task_id
      }
    }
  };
}
