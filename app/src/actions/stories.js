export const FETCH_STORIES = 'stories/fetch_stories';
export const ADD_STORY = 'stories/add';
export const UPDATE_STORY = 'stories/update';
export const DELETE_STORY = 'stories/delete';

import KanbanService from '../services/KanbanService';

export function fetchStories() {
  return {
    type: FETCH_STORIES,
    payload: KanbanService.getStories()
  };
}

export function addStory(story) {
  return {
    type: ADD_STORY,
    payload: story
  };
}

export function updateStory(story) {
  return {
    type: UPDATE_STORY,
    payload: KanbanService.updateStory(story.id, story)
  };
}

export function deleteStory(story_id) {
  return {
    type: DELETE_STORY,
    payload: KanbanService.deleteStory(story_id),
    successAction: {
      type: DELETE_STORY,
      payload: {
        id: story_id
      }
    }
  };
}
