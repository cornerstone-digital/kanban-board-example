import ApiHandler from '../handlers/api';

class KanbanService {
  /* TASK METHODS */
  addTask(task) {
    return new Promise((resolve, reject) => {
      ApiHandler.post('/tasks', task)
        .then(task => {
          return resolve(task);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  getTaskById(task_id) {
    return new Promise((resolve, reject) => {
      ApiHandler.get('/tasks/' + task_id)
        .then(task => {
          return resolve(task);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  getTasks(filters = {}) {
    return new Promise((resolve, reject) => {
      ApiHandler.get('/tasks')
        .then(tasks => {
          if(filters.story_id) {
            const filteredTasks = tasks.filter(task => task.story_id === filters.story_id);
            return resolve(filteredTasks);
          }

          return resolve(tasks);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  updateTask(task_id, task) {
    return new Promise((resolve, reject) => {
      ApiHandler.patch('/tasks/' + task_id, task)
        .then(response => {
          return resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  deleteTask(task_id) {
    return new Promise((resolve, reject) => {
      ApiHandler.delete('/tasks/' + task_id)
        .then(() => {
          return resolve();
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  /* STORY METHODS */

  addStory(story) {
    return new Promise((resolve, reject) => {
      ApiHandler.post('/stories', story)
        .then(story => {
          return resolve(story);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  getStoryById(story_id) {
    return new Promise((resolve, reject) => {
      ApiHandler.get('/stories/' + story_id)
        .then(story => {
          return resolve(story);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  getStories() {
    return new Promise((resolve, reject) => {
      ApiHandler.get('/stories')
        .then(stories => {
          return resolve(stories);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  updateStory(story_id, story) {
    return new Promise((resolve, reject) => {
      ApiHandler.patch('/stories/' + story_id, story)
        .then(response => {

          return resolve(response);
        })
        .catch(error => {
          reject(error);
        })
    });
  }

  deleteStory(story_id) {
    return new Promise((resolve, reject) => {
      ApiHandler.delete('/stories/' + story_id)
        .then(() => {
          return resolve();
        })
        .catch(error => {
          reject(error);
        })
    });
  }
}

export default new KanbanService();
