import { combineReducers } from 'redux';
import notificationReducer from './_inc/notifications';
import storyReducer from './_inc/stories';
import taskReducer from './_inc/tasks';

const rootReducer = combineReducers({
  notification: notificationReducer,
  stories: storyReducer,
  tasks: taskReducer
});

export default rootReducer;
