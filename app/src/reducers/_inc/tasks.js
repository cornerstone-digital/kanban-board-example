import { FETCH_TASKS, ADD_TASK, UPDATE_TASK, DELETE_TASK } from '../../actions/tasks';
export default function (state = [], action) {
  switch (action.type) {
    case FETCH_TASKS:
      return action.payload;
    case ADD_TASK:
      return [
        ...state,
        action.payload
      ];
    case UPDATE_TASK:
      return state.map(task => {
        if(task.id !== action.payload.id) {
          return task;
        }

        return action.payload;
      });

      break;
    case DELETE_TASK:
      return state.filter(task => task.id !== action.payload.id);
      break;
    default:
      return state;
  }
}
