import { FETCH_STORIES, ADD_STORY, UPDATE_STORY, DELETE_STORY } from '../../actions/stories';

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_STORIES:
      return action.payload;
    case ADD_STORY:
      return [
        ...state,
        action.payload
      ];
    case UPDATE_STORY:
      return state.map(story => {
        if(story.id !== action.payload.id) {
          return story;
        }

        return action.payload;
      });
    case DELETE_STORY:
      return state.filter(story => story.id !== action.payload.id);
    default:
      return state;
  }
}
